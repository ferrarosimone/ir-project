#La sezione “Tfidf” è stata realizzata da Gregorio Cadolo con la revisione di Simone Ferraro.
import math

tokenize = lambda doc: doc.lower().split(" ")                                           # Lambda function che tokenizza il documento

def InversDocumentFrequencies(tokenizedDocuments):                                      #IDF
    idfValues = {}                                                                      #token - valore idf
    tokensSet = set([item for sublist in tokenizedDocuments for item in sublist])       #set parole tokenizzate
    for tkn in tokensSet:
        if(tkn != ""):
            tokenRepository = map(lambda doc: tkn in doc, tokenizedDocuments)           #memorizza le parole tokenizzate
            idfValues[tkn] = 1 + math.log(len(tokenizedDocuments)/(sum(tokenRepository)))
    return idfValues

# Calcola la frequenza del termine nel documento
def termFrequency(term, documentTokenized):
    return documentTokenized.count(term)

# Calcola i valori tfidf del documento per ogni categoria
def tfidfFunction(documents, namesetEmoticon):
    out = {
        'Rabbia': [],
        'Paura': [],
        'Sorpresa': [],
        'Gioia': [],
        'Disgusto': [],
        'Tristezza': [],
        'NoEmoticon': []
    }
    documentTokenized = [tokenize(d) for d in documents]            # Tokenizza le parole del documento
    idf = InversDocumentFrequencies(documentTokenized)              # Calcola i valori idf
    tfidfDocuments = []
    for indexCategory, document in enumerate(documentTokenized):
        wordTfidf = []
        for term in idf.keys():
            tf = termFrequency(term, document)
            wordTfidf.append((term, tf * idf[term]))
        out[namesetEmoticon[indexCategory]] = wordTfidf;
        tfidfDocuments.append(wordTfidf)
    return tfidfDocuments, out
