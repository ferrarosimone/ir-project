#La sezione “Utilities” è stata realizzata da Gregorio Cadolo e Simone Ferraro.
import tweepy
import re
import requests
from bs4 import BeautifulSoup

#Created by Simone
#Connessione alle API
def connectionToAPI():
    consumerKey = 'XFlElvmXrkEEp7SKVN4K3dNc4'
    consumerSecret = 'Q5dkq7UnSXGBYD3FBAdRSZojrkOvJUwGmBxZmRpe18AtbO6bUs'
    auth = tweepy.OAuthHandler(consumerKey, consumerSecret)
    return tweepy.API(auth)

#Created by Gregorio
#Funzione che categorizza il tweet
def categorizeTweet(text, namesetEmoticon):
    datasetEmoticon = [
        "😠😡🤬😤👿😾",
        "😟😨😰",
        "😱🙀😮🤩😵😲🤯",
        "😀😁😂😃😄🤣😆😇😉😊🙂🙃☺️😜🤪😝😺😸😹😻",
        "😣😖🤢😷💩",
        "😞😟😔😕😫😩😦😧😢😥😪"
    ]
    # l'accuratezza all'inizio è 0
    counter = [0,0,0,0,0,0]
    for index, category in enumerate(datasetEmoticon):
        for symbol in datasetEmoticon[index]:
            counter[index] += text.count(symbol)

    # Ricavo la categoria principale e la sua percentuale di accuratezza
    if(sum(counter) == 0):
        main_category = 'NoEmoticon'
        accuracity = 100
    else:
        main_category = namesetEmoticon[counter.index(max(counter))]
        accuracity = (max(counter) / sum(counter)) * 100
    return main_category, accuracity

def findLinkinText(text):
    return re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)

def clearLinkText(text):
    return re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '', text, flags=re.MULTILINE)

def clearText(text):
    return re.sub(r'[^\w\s]', '', text, re.UNICODE).replace("\n", " ").replace("\t", "").replace("\r", "").strip(" ")

#Created by Simone
#rimuove le stopword all'interno dei documenti
def removeStopWords(text):
    stopWordsEn = ['i','the', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', 'her', 'hers', 'herself', 'it', 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', 'should', 'now']
    out = ""
    for word in text.split(" "):
        if word.lower() not in stopWordsEn:
            out = out + " " + word
    return out.strip()

# Created by Simone
# Categorizza le parole contenute nei tweet
def textListCategory(tweetUser):
    out = {
        'Rabbia' : "",
        'Paura' : "",
        'Sorpresa' : "",
        'Gioia' : "",
        'Disgusto' : "",
        'Tristezza' : "",
        'NoEmoticon' : ""
    }
    #Viene creato un dizionario con key il sentimento e value il merge di tutti i tweet di quella categoria
    #Li accorpiamo per far si che il tfidf indichi l'importanza della parola nella categoria invece del singolo tweet.
    for t in tweetUser:
        out[t.category] = out[t.category] + " " + t.text
    return out

# Created by Simone
# Prendo le parole delle pagine web scaricate
def linkTextListCategory(tweetUser):
    out = {
        'Rabbia' : "",
        'Paura' : "",
        'Sorpresa' : "",
        'Gioia' : "",
        'Disgusto' : "",
        'Tristezza' : "",
        'NoEmoticon' : ""
    }
    for t in tweetUser:
        if (len(t.link) > 0):
            for link in t.link:
                out[t.category] = out[t.category] + " " + getTextFromUrl(link)  #crea una lista di testi (paragrafi <p> in html)
    return out

# Created by Gregorio
# Restituisce le parole con tfidf più alto
def getTopNWords(dataset, nWords, namesetEmoticon):
    out = {
        'Rabbia': [],
        'Paura': [],
        'Sorpresa': [],
        'Gioia': [],
        'Disgusto': [],
        'Tristezza': [],
        'NoEmoticon': []
    }
    for category in namesetEmoticon:
        arraySorted = sorted(dataset[category], key=lambda tup: tup[1], reverse=True)
        mainKeywords = []
        for i in range(nWords):
            mainKeywords.append(arraySorted[i][0])
        out[category] = mainKeywords
    return out

# Created by Gregorio
# ricava l'url dal testo
def getTextFromUrl(url):
    pageContent = requests.get(url).content
    soup = BeautifulSoup(pageContent, 'html.parser')
    pTag = [tag.get_text() for tag in soup.find_all('p')]
    text = ""

    for p in pTag:
        text = text + " " + p
    text = removeStopWords(clearText(clearLinkText(text)))
    return text

# Created by Simone
# Stampa dell'output formattato
def printOutput(keywords, keywordsUrl, namesetEmoticon, printToFile = False):
    out={}

    for key in namesetEmoticon:
        print(key, ": ", keywords[key], ": ", keywordsUrl[key])

    if printToFile == True:
        f = open("Output.txt", "a")
        for key in namesetEmoticon:
            f.write(key + ": " + str(keywords[key]) + ": " + str(keywordsUrl[key]) + "\n")
        f.close()