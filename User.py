#La sezione “Tweet” è stata realizzata da Gregorio Cadolo e Simone Ferraro.
from Tweet import Tweet
from Utilities import *
from Tfidf import *

class User:

    def __init__(self, userName, namesetEmoticon, api):
        self.namesetEmoticon = namesetEmoticon
        self.userName = userName
        listTweetsUser = api. user_timeline(userName)                                                   # Timeline di un utente
        self.tweets = [Tweet(t._json["id"], t._json["text"], namesetEmoticon) for t in listTweetsUser]  # Lista di oggetti di tipo Tweet

        # Otteniamo un dizionario contenente come chiave il sentimento e come valore una concatenazione di parole presenti nei tweets
        tweetsDict = textListCategory(self.tweets)
        # Crea un array di 7 stringhe ognuna della quali è la concatenazione della varie frasi divise per sentimento
        self.lectures = [tweetsDict[c] for c in namesetEmoticon]

        # Restituisce le parole contenute nelle pagine web scaricate e categorizzate
        tweetsDictUrl = linkTextListCategory(self.tweets)
        # Crea un array di 7 stringhe ognuna della quali è la concatenazione della varie frasi divise per sentimento
        self.lecturesUrl = [tweetsDictUrl[c] for c in namesetEmoticon]

    # Calcola i valori tfidf dei tweet e dei documenti scaricati
    def tfidf(self):
        lecturesTfIdf = tfidfFunction(self.lectures, self.namesetEmoticon)[1]
        lecturesUrlTfIdf = tfidfFunction(self.lecturesUrl, self.namesetEmoticon)[1]
        return lecturesTfIdf, lecturesUrlTfIdf

    # Restituisce le parole con valore tfidf più alto sia dei tweet che dei documenti scaricati
    def tfidfGetTopWords(self, lecturesTfIdf,lecturesUrlTfIdf, nWord):
        keywords = getTopNWords(lecturesTfIdf, nWord, self.namesetEmoticon)
        keywordsUrl = getTopNWords(lecturesUrlTfIdf, nWord, self.namesetEmoticon)
        return keywords, keywordsUrl
