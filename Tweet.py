#La sezione “Tweet” è stata realizzata da Gregorio Cadolo e Simone Ferraro.
from Utilities import *

class Tweet:
    def __init__(self, id, text, namesetEmoticon):
        self.id = id
        self.link = findLinkinText(text) #Si analizza il testo per trovare i link in esso contenuti
        text = clearLinkText(text)       #si cancellano i link dal testo per rendere l'elaborazione più semplice
        text = removeStopWords(text)     #si cancellano le stopWords dal testo
        self.text = clearText(text)      #si cancellano gli eventuali simboli dal testo per rendere l'elaborazione più semplice
        # Restituisce la categoria del tweet analizzato e la percentuale di accuratezza
        self.category, self.accuracity = categorizeTweet(text, namesetEmoticon)