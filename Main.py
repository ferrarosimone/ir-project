#La sezione “Main” è stata realizzata da Gregorio Cadolo e Simone Ferraro.
from Utilities import *
from JaccardSimilarity import *
from User import User

printToFile = True
nWord = 5                                                                                       # Top Word nel testo analizzato
namesetEmoticon = ['Rabbia', 'Paura', 'Sorpresa', 'Gioia', 'Disgusto', 'Tristezza', 'NoEmoticon']
api = connectionToAPI()                                                                         # Connessione alle API di Twitter

# Dichiariamo i due utenti
simone = User("SimoneFerraro4",namesetEmoticon, api)
greg = User("gregoriocandolo",namesetEmoticon, api)
users = [simone, greg]

for user in users:
    print("\nUser >", user.userName, "\n")
    lecturesTfIdf, lecturesUrlTfIdf = user.tfidf()                                              # Calcola i valori di tfidf delle documenti dell'utente
    keywords, keywordsUrl = user.tfidfGetTopWords(lecturesTfIdf, lecturesUrlTfIdf, nWord)       # Ricava le n parole con i valori tfidf più alti
    printOutput(keywords, keywordsUrl, namesetEmoticon, printToFile)                            # Formatta l'output nel formato richiesto
    JaccardSimilarityEmotion(keywords, keywordsUrl, namesetEmoticon)                            # Calcola la similarità di jaccard tra i tweet e le pagine web scaricate per ogni sentimento

jaccardSimilarityUsers(simone, greg, namesetEmoticon, nWord)                                    # Confronta le top words fra due utenti

'''
#Javascript disabilitato
for tweet in greg.tweets:
    for link in tweet.link:
        print(getTextFromUrl(link))
'''