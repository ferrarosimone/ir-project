#La sezione “JaccardSimilarity” è stata realizzata da Simone Ferraro con la revisione di Gregorio Candolo.

# Fuzione della Jaccard Similarity
def jaccard_similarity(query, document):
    intersection = set(query).intersection(set(document))
    union = set(query).union(set(document))
    return len(intersection)/len(union)

# Funzione che prende le parole dei tweet e delle pagine scaricate e restituisce la Jaccard Similarity
def JaccardSimilarityEmotion(keywords, keywordsUrl, namesetEmoticon):
    for emotion in namesetEmoticon:
        if emotion not in namesetEmoticon:
            print("Emotion not valid")
            exit("404: Emotion not Found")
        list1 = keywords[emotion]
        list2 = keywordsUrl[emotion]
        print("The Jaccard similarity for ", emotion, "is ", jaccard_similarity(list1, list2))

# Lamda Function che tockenizza le parole di un documento passato (split e lower case)
tokenize = lambda doc: doc.lower().split(" ")

# Jaccard similarity tra utenti
def jaccardSimilarityUsers(user1, user2, namesetEmoticon, nWord):
    # Utente 1
    lecturesTfIdf, lecturesUrlTfIdf = user1.tfidf()
    keywords_user1, keywordsUrl_user1 = user1.tfidfGetTopWords(lecturesTfIdf, lecturesUrlTfIdf, nWord)

    # Utente 2
    lecturesTfIdf, lecturesUrlTfIdf = user2.tfidf()
    keywords_user2, keywordsUrl_user2 = user2.tfidfGetTopWords(lecturesTfIdf, lecturesUrlTfIdf, nWord)

    print("\nConfronto fra", user1.userName, "e", user2.userName, "\n")

    for index, sentiment in enumerate(namesetEmoticon):
        # Calcolo la similarità tra i testi dei tweet dei due utenti
        tweetTextSimilarity = jaccard_similarity(keywords_user1[sentiment], keywords_user2[sentiment])
        # Calcolo la similarità tra i contenuti delle pagine web scaricate
        webTextSimilarity = jaccard_similarity(keywordsUrl_user1[sentiment], keywordsUrl_user2[sentiment])

        print(sentiment, ": ", tweetTextSimilarity, ": ", webTextSimilarity)